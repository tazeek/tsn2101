****
****

## Subject: Operating Systems, TSN2101 ##
## Lecturer: Timothy Yap Tzen Vun ##
## Lecture Section: TC01 ##


* PROJECT: CPU Scheduling Algorithms
* PROJECT DUE: 23rd September 2015
* EARLIEST POSSIBLE FINISH: 5th-7th August

## SOFTWARE TO USE ##

* Git Bash
* SourceTree with Bitbucket Account 
* Text Editor (Ex. Sublime Text)
* NOTE: Can use only BASH SHELL SCRIPTS

## MEMBERS ##

* Chong Hon Siong - 1131123100
* Farhanuddin Bin Hamdan - 1132702175
* Khor Huai Qian - 1132702164
* Tazeek Bin Abdur Rakib - 1131122661

## ALGORITHMS ##

1. First Come First Served Pre-emptive Priority(Ice)
2. Round Robin Scheduling (Marak)
3. Three-level Queue Scheduling (Anonoz)
4. Shortest Remaining Time Next Scheduling (Tazeek)



## USER INPUT ##

* Arrival Time (All Algorithms)
* Burst Time (All Algorithms)
* Priority (Algorithm 1 & Algorithm 3 ONLY, Priority ranges from 1 to 6 with smaller number indicating higher priority)
* Time Quantum (Algorithm 2 & Algorithm 3 ONLY)
* NOTE: Number of Processes can range from 3 to 10. 

## EXPECTED OUTPUT ##

* Gantt Chart (Use Mr.Timothy's Version)
* Average Turnaround Time of EACH ALGORITHM
* Average Waiting Time of EACH ALGORITHM